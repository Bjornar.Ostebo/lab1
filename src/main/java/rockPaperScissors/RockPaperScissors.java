package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {

    public static void main(String[] args) {
        /*
         * The code here does two things:
         * It first creates a new RockPaperScissors -object with the
         * code `new RockPaperScissors()`. Then it calls the `run()`
         * method on the newly created object.
         */
        new RockPaperScissors().run();
    }

    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    Map<String, Map<String, Integer>> outcomes = Map.of(
            "rock", Map.of("rock", 0, "paper", -1, "scissors", 1),
            "paper", Map.of("rock", 1, "paper", 0, "scissors", -1),
            "scissors", Map.of("rock", -1, "paper", 1, "scissors", 0));

    public void run() {
        game_loop: while (true) {
            System.out.println("Let's play round " + roundCounter);
            String computerChoice = computerChoice();
            String humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            while (!validate(humanChoice)) {
                System.out.println("I do not understand " + humanChoice + ". Could you try again?");
                humanChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            }
            int result = play(humanChoice, computerChoice);
            printResult(humanChoice, computerChoice, result);
            updateAndPrintScore(result);

            String continuePlay = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            while (!(continuePlay.equals("n") || continuePlay.equals("y"))) {
                System.out.println("I do not understand " + continuePlay + ". Could you try again?");
                continuePlay = readInput("Do you wish to continue playing? (y/n)?").toLowerCase();
            }
            if (continuePlay.equals("n")) {
                System.out.println("Bye bye :)");
                break game_loop;
            } else {
                roundCounter++;
            }
        }
    }

    /**
     * Updates and prints the score
     * 
     * @params the result of the play
     */
    public void updateAndPrintScore(int result) {
        if (result == 1) {
            humanScore++;
        } else if (result == -1) {
            computerScore++;
        }
        System.out.println("Score: human " + humanScore + ", computer " + computerScore);
    }

    /**
     * Prints the outcome of the play
     * 
     * @params the human's choice, the computer's choice and the result of the play
     */
    public void printResult(String humanChoice, String computerChoice, int result) {
        String message = "";
        if (result == 1) {
            message = "Human wins!";
        } else if (result == -1) {
            message = "Computer wins!";
        } else {
            message = "It's a tie!";
        }
        System.out.println("Human chose " + humanChoice + ", computer chose " +
                computerChoice + ". " + message);
    }

    /**
     * "Plays" the game, and returns number representing the outcome
     * 
     * @params the human's choice and the computer's choice
     * @return 0 = tie, 1 = win, -1 = loss
     */
    public int play(String humanChoice, String computerChoice) {
        return outcomes.get(humanChoice).get(computerChoice);
    }

    /**
     * Randomly returns a choice between rock, paper and scissors
     * 
     * @return a random choice
     */
    public String computerChoice() {
        return rpsChoices.get(new Random().nextInt(rpsChoices.size()));
    }

    /**
     * Validates choice (should be rock, paper or scissors)
     * 
     * @param humanChoice
     * @return true if valid, false if not
     */
    public boolean validate(String humanChoice) {
        return rpsChoices.contains(humanChoice);
    }

    /**
     * Reads input from console with given prompt
     * 
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
